package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strings"

	"gitee.com/yunind/practice/mulver/example/echo"
	"gitee.com/yunind/practice/mulver/internal/util"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/status"
)

type server struct {
}

func (s *server) Fly(ctx context.Context, in *echo.Offset) (*echo.Offset, error) {
	return &echo.Offset{X: 10, Y: in.Y + 1}, nil
	// return nil, status.Error(codes.Unimplemented, "unimplement")
}

type healthServer struct{}

func (s *healthServer) Check(ctx context.Context, in *healthpb.HealthCheckRequest) (*healthpb.HealthCheckResponse, error) {
	log.Printf("Handling grpc Check request")
	return &healthpb.HealthCheckResponse{Status: healthpb.HealthCheckResponse_SERVING}, nil
}

// Watch is not implemented
func (s *healthServer) Watch(in *healthpb.HealthCheckRequest, srv healthpb.Health_WatchServer) error {
	return status.Error(codes.Unimplemented, "Watch is not implemented")
}

func main() {
	grpcport := ":5080"
	http.HandleFunc("/", hchandler)
	http.HandleFunc("/_ah/health", hchandler)

	sopts := []grpc.ServerOption{grpc.MaxConcurrentStreams(10)}
	s := grpc.NewServer(sopts...)
	echo.RegisterEchoServerServer(s, &server{})
	healthpb.RegisterHealthServer(s, &healthServer{})

	muxHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if util.IsGrpcRequest(r) {
			if p := strings.TrimPrefix(r.URL.Path, "/v1"); len(p) < len(r.URL.Path) {
				r2 := new(http.Request)
				*r2 = *r
				*r2.URL = *r.URL
				r2.URL.Path = p
				s.ServeHTTP(w, r)
				return
			}
			return
		}
		http.DefaultServeMux.ServeHTTP(w, r)
	})
	log.Printf("Starting gRPC server on port %s", grpcport)

	log.Fatal(http.ListenAndServe(grpcport, h2c.NewHandler(muxHandler, &http2.Server{})))
}

func hchandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "ok")
}
