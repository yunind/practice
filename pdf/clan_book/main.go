package main

import (
	"log"
	"os"
	"path/filepath"

	"gitee.com/yunind/practice/pdf/clan_book/servers/clans/models"
)

func main() {
	currentPath, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	testM1 := models.Member{
		Name:   "测试",
		Status: 9,
		Children: []*models.Member{
			&models.Member{
				Name:   "测试",
				Status: 10,
				Children: []*models.Member{
					&models.Member{
						Name:   "测试",
						Status: 11,
						Children: []*models.Member{
							&models.Member{
								Name:   "测试",
								Status: 12,
								Children: []*models.Member{
									&models.Member{
										Name:   "测试",
										Status: 13,
										Children: []*models.Member{
											&models.Member{
												Name:   "测试",
												Status: 14,
												Children: []*models.Member{
													&models.Member{
														Name:   "测试",
														Status: 15,
														Children: []*models.Member{
															&models.Member{
																Name:   "测试",
																Status: 16,
																Children: []*models.Member{
																	&models.Member{
																		Name:   "测试",
																		Status: 17,
																		Children: []*models.Member{
																			&models.Member{
																				Name:   "测试",
																				Status: 18,
																				Children: []*models.Member{
																					&models.Member{
																						Name:   "测试",
																						Status: 19,
																						Children: []*models.Member{
																							&models.Member{
																								Name:   "测试",
																								Status: 20,
																								Children: []*models.Member{
																									&models.Member{
																										Name:   "测试",
																										Status: 21,
																									},
																								},
																							},
																						},
																					},
																				},
																			},
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	testM2 := models.Member{
		Name:   "测试2",
		Status: 9,
		Children: []*models.Member{
			&models.Member{
				Name:   "测试2",
				Status: 10,
				Children: []*models.Member{
					&models.Member{
						Name:   "测试2",
						Status: 11,
						Children: []*models.Member{
							&models.Member{
								Name:   "测试2",
								Status: 12,
								Children: []*models.Member{
									&models.Member{
										Name:   "测试2",
										Status: 13,
										Children: []*models.Member{
											&models.Member{
												Name:   "测试2",
												Status: 14,
												Children: []*models.Member{
													&models.Member{
														Name:   "测试2",
														Status: 15,
														Children: []*models.Member{
															&models.Member{
																Name:   "测试2",
																Status: 16,
																Children: []*models.Member{
																	&models.Member{
																		Name:   "测试2",
																		Status: 17,
																		Children: []*models.Member{
																			&models.Member{
																				Name:   "测试2",
																				Status: 18,
																				Children: []*models.Member{
																					&models.Member{
																						Name:   "测试2",
																						Status: 19,
																						Children: []*models.Member{
																							&models.Member{
																								Name:   "测试2",
																								Status: 20,
																								Children: []*models.Member{
																									&models.Member{
																										Name:   "测试2",
																										Status: 21,
																									},
																								},
																							},
																						},
																					},
																				},
																			},
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	testCai := &models.Member{
		Name:    "才",
		Surname: "覃",
		Status:  9,
		Children: []*models.Member{
			&models.Member{
				Name:   "象英",
				Status: 10,
				Children: []*models.Member{
					&models.Member{
						Name:   "圣光",
						Status: 11,
					},
					&models.Member{
						Name:   "圣耀",
						Status: 11,
						Children: []*models.Member{
							&models.Member{
								Name:   "志仲",
								Status: 12,
								Children: []*models.Member{
									&models.Member{
										Name:   "汝杰",
										Status: 13,
									},
									&models.Member{
										Name:   "渭杰",
										Status: 13,
									},
									&models.Member{
										Name:   "沃杰",
										Status: 13,
									},
									&models.Member{
										Name:   "清杰",
										Status: 13,
									},
								},
							},
						},
					},
					&models.Member{
						Name:   "圣昌",
						Status: 11,
					},
				},
			},
		},
	}
	testShiKao := &models.Member{
		Name:   "中间失考1",
		Status: 9,
		Children: []*models.Member{
			&models.Member{
				Name:   "中间失考2",
				Status: 10,
				Children: []*models.Member{
					&models.Member{
						Name:   "失考3",
						Status: 11,
						Children: []*models.Member{
							&models.Member{
								Name:   "失考4",
								Status: 12,
								Children: []*models.Member{
									&models.Member{
										Name:   "失考5",
										Status: 13,
										Children: []*models.Member{
											&models.Member{
												Name:   "失考6",
												Status: 14,
												Children: []*models.Member{
													&models.Member{
														Name:   "失考7",
														Status: 15,
														Children: []*models.Member{
															&models.Member{
																Name:   "失考8",
																Status: 16,
																Children: []*models.Member{
																	&models.Member{
																		Name:   "失考9",
																		Status: 17,
																		Children: []*models.Member{
																			&models.Member{
																				Name:   "失考10",
																				Status: 18,
																				Children: []*models.Member{
																					&models.Member{
																						Name:   "友文",
																						Status: 19,
																					},
																					&models.Member{
																						Name:   "友光",
																						Status: 19,
																					},
																					&models.Member{
																						Name:   "友学",
																						Status: 19,
																					},
																					&models.Member{
																						Name:   "友祥",
																						Status: 19,
																					},
																					&models.Member{
																						Name:   "友建",
																						Status: 19,
																					},
																				},
																			},
																			&models.Member{
																				Name:   "失考11",
																				Status: 18,
																				Children: []*models.Member{
																					&models.Member{
																						Name:   "友和",
																						Status: 19,
																					},
																				},
																			},
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	testM := &models.Member{
		Name:   "文泰",
		Status: 1,
		Spouse: &models.MemberRelationship{
			Person: &models.Person{
				Name: "李氏",
			},
		},
		Children: []*models.Member{
			&models.Member{
				Name:   "广泽",
				Status: 2,
				Children: []*models.Member{
					&models.Member{
						Name:   "允通",
						Status: 3,
						Children: []*models.Member{
							&models.Member{
								Name:   "永禄",
								Status: 4,
								Spouse: &models.MemberRelationship{
									Person: &models.Person{
										Name: "测氏",
									},
								},
								Children: []*models.Member{

									&models.Member{
										Name:   "思洪",
										Status: 5,
										Children: []*models.Member{
											&models.Member{
												Name:   "绍熙",
												Status: 6,
												Children: []*models.Member{
													&models.Member{
														Name:   "懿容",
														Status: 7,
													},
												},
											},
										},
									},
									&models.Member{
										Name:   "思吕",
										Status: 5,
										Children: []*models.Member{
											&models.Member{
												Name:   "绍镇",
												Status: 6,
												Children: []*models.Member{
													&models.Member{
														Name:   "懿朝",
														Status: 7,
														Children: []*models.Member{
															&models.Member{
																Name:   "步业",
																Status: 8,
															},
														},
													},
												},
											},
										},
									},
									&models.Member{
										Name:   "思廉",
										Status: 5,
										Children: []*models.Member{
											&models.Member{
												Name:   "绍虞",
												Status: 6,
												Children: []*models.Member{
													&models.Member{
														Name:   "懿芳",
														Status: 7,
														Spouse: &models.MemberRelationship{
															Person: &models.Person{
																Name: "测氏",
															},
														},
														Children: []*models.Member{
															&models.Member{
																Name:   "建业",
																Status: 8,
																Children: []*models.Member{
																	testCai,
																},
															},
															&models.Member{
																Name:   "展业",
																Status: 8,
																Children: []*models.Member{
																	testShiKao,
																},
															},
															&models.Member{
																Name:   "舒业",
																Status: 8,
																Children: []*models.Member{
																	&testM1,
																},
															},
														},
													},
													&models.Member{
														Name:   "懿培",
														Status: 7,
														Children: []*models.Member{
															&models.Member{
																Name:   "测试",
																Status: 8,
																Children: []*models.Member{
																	&testM2,
																},
															},
														},
													},
												},
											},
										},
									},
									&models.Member{
										Name:   "思旦",
										Status: 5,
									},
								},
							},
						},
					},
				},
			},
		},
	}

	cb := models.NewClanBook(1, "代", 1, currentPath)
	cb.WifeNamesIndent = 5
	cb.LineHeight = 5
	// err = cb.Write(filepath.Join(currentPath, "test.pdf"), testM)
	p := models.CreatePrintProgress()
	err = cb.WriteWithProgress(filepath.Join(currentPath, "test.pdf"), cb.MemberConv(testM, 0, p), p)
	if err != nil {
		panic(err)
	}
	for {
		select {
		case msg := <-p.Message:
			log.Printf("%s: %d\n", msg.Message, msg.Index*100/p.Total)
		case <-p.Done():
			return
		}
	}
}
