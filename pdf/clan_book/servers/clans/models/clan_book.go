package models

import (
	"fmt"
	"log"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"github.com/signintech/gopdf"
)

// Padding for name
type Padding struct {
	Top, Right, Buttom, Left float64
}

// FontStyle font style for print clan
type FontStyle struct {
	FontSize          int
	FontSizeForWife   int
	FontSizeForStatus int
}

// StyleType font style type
type StyleType int

const (
	_ StyleType = iota - 1
	// FNor normal font style
	FNor
	// FMin minimal font style
	FMin
	// FMax max font style
	FMax
)

type rectQueue struct {
	status int32
	mrs    []*memberRect
}

type rectSlice []*rectQueue

func (rs rectSlice) Len() int {
	return len(rs)
}

func (rs rectSlice) Swap(i, j int) {
	rs[i], rs[j] = rs[j], rs[i]
}

func (rs rectSlice) Less(i, j int) bool {
	return rs[i].status < rs[j].status
}

// ClanBookPrinter 族谱书籍打印
type ClanBookPrinter struct {
	pdf        *gopdf.GoPdf
	statusName string
	// perStatusHeight float64
	// MinStyle minimal font style
	MinStyle *FontStyle
	// NorStyle normal font style
	NorStyle *FontStyle
	// MaxStyle max font style
	MaxStyle       *FontStyle
	fontPath       string //directory of fonts
	FontSizeForNum int    //用于打印页码
	fromStatus     int32
	lastStatus     int32 //cal
	// offsetStatus    int32   //代数偏移
	WifeNamesIndent float64 //配偶姓名缩进
	LineHeight      float64 //竖线长度

	Padding          Padding //内间距
	pageLong         float64 //长边长度
	pageShort        float64 //短边长度
	PagePadding      Padding //页面内边距,同时用于族谱块内边距
	pages            map[int]*clanPage
	currentPageIndex int //当前页码
	pageStartNum     int //开始页码
	queue            rectSlice

	// progress
	progress *PrintProgress //生成进度

}

type area struct {
	startX, startY float64
	width, height  float64
}

type clanPage struct {
	index     int       //页码
	direction direction //是否竖向
	ver       area
	hor       area
	pointX    float64 // 相交矩形的内点x轴值
	pointY    float64 // 相交矩形的内点y轴值
	mrs       []*memberRect
	// pms []*pageMember
	// currentHeight float64 //当前遍历高度
	currentWidth float64 //当前遍历宽度
	dispached    bool    //已完成
}

type direction int

const (
	_ direction = iota - 1
	horizonal
	vertical
)

type memberRect struct {
	// useWidth     float64 //截止使用的宽度
	member       *PdfMember
	statusHeight map[int32]float64 //代次高度
	toStatus     int32             //截止代次
	// norWidth rect width when normal font style
	norWidth float64
	// norHeight rect height when normal font style
	norHeight float64
	startX    float64
	startY    float64
	direction direction
}

func (cp *memberRect) calStatusHeight(currentStatus int32) float64 {
	shs := cp.statusHeight
	h := 0.0
	for i := cp.member.Status; i <= currentStatus; i++ {
		h += shs[i]
	}
	return h
}

// NewClanBook create new clan book
func NewClanBook(fromStatus int32, statusName string, startPageNum int, fontPath string) *ClanBookPrinter {
	pdf := gopdf.GoPdf{}
	// 默认为横向A4纸
	pdf.Start(gopdf.Config{PageSize: gopdf.Rect{H: 595, W: 842}})
	return &ClanBookPrinter{
		pdf:              &pdf,
		fromStatus:       fromStatus,
		statusName:       statusName,
		fontPath:         fontPath,
		FontSizeForNum:   8,
		WifeNamesIndent:  10,
		Padding:          Padding{5.0, 5.0, 5.0, 5.0},
		PagePadding:      Padding{8.0, 8.0, 5.0, 5.0},
		pageLong:         842,
		pageShort:        595,
		pages:            make(map[int]*clanPage),
		currentPageIndex: startPageNum - 1, //未创建时
		pageStartNum:     startPageNum,
		MinStyle: &FontStyle{
			FontSize:          12,
			FontSizeForWife:   10,
			FontSizeForStatus: 12,
		},
		NorStyle: &FontStyle{
			FontSize:          14,
			FontSizeForWife:   10,
			FontSizeForStatus: 14,
		},
		MaxStyle: &FontStyle{
			FontSize:          16,
			FontSizeForWife:   12,
			FontSizeForStatus: 16,
		},
	}
}

// WriteWithProgress 附带进度提示
func (cbp *ClanBookPrinter) WriteWithProgress(path string, pm *PdfMember, p *PrintProgress) error {
	if p == nil {
		panic("print progress is nil")
	}
	err := cbp.pdf.AddTTFFont("阿里", filepath.Join(cbp.fontPath, "Alibaba-PuHuiTi-Medium.ttf"))
	if err != nil {
		return err
	}

	cbp.progress = p
	go func() {
		cbp.deepCal(pm)
		cbp.designRect(pm, nil)
		cbp.dispatchPage()
		cbp.printPages()
		close(cbp.progress.done)
	}()
	go func() {
		<-cbp.progress.done
		if cbp.progress.closed {
			return
		}
		log.Println("write pdf")
		err = cbp.pdf.WritePdf(path)
		if err != nil {
			log.Println(err)
		}
	}()

	return err
}

func (cbp *ClanBookPrinter) Write(path string, member *Member) error {

	err := cbp.pdf.AddTTFFont("阿里", filepath.Join(cbp.fontPath, "Alibaba-PuHuiTi-Medium.ttf"))
	if err != nil {
		return err
	}

	// 从世代开始 计算偏移
	offsetStatus := cbp.fromStatus - member.Status
	pm := cbp.MemberConv(member, offsetStatus, nil)
	cbp.deepCal(pm)
	cbp.designRect(pm, nil)
	cbp.dispatchPage()
	cbp.printPages()

	err = cbp.pdf.WritePdf(path)
	return err
}

func (cbp *ClanBookPrinter) useFont(style *FontStyle) {
	var err error
	if style != nil {
		err = cbp.pdf.SetFont("阿里", "", style.FontSize)
	} else {
		err = cbp.pdf.SetFont("阿里", "", cbp.NorStyle.FontSize)
	}

	if err != nil {
		panic(err)
	}
}

func (cbp *ClanBookPrinter) useFontForWife(style *FontStyle) {
	var err error
	if style != nil {
		err = cbp.pdf.SetFont("阿里", "", style.FontSizeForWife)
	} else {
		err = cbp.pdf.SetFont("阿里", "", cbp.NorStyle.FontSizeForWife)
	}

	if err != nil {
		panic(err)
	}
}

func (cbp *ClanBookPrinter) useFontForPageNum() {
	err := cbp.pdf.SetFont("阿里", "", cbp.FontSizeForNum)
	if err != nil {
		panic(err)
	}

}

// MemberConv convert member to pdf's member
func (cbp *ClanBookPrinter) MemberConv(member *Member, offsetStatus int32, p *PrintProgress) *PdfMember {
	if member.Status == 0 {
		panic("member status is 0")
	}
	name := []rune(member.Name)
	if len(name) == 1 {
		name = append([]rune(member.Surname), name...)
	}
	if strings.Contains(member.Name, "失考") {
		name = []rune("失考")
	}

	current := &PdfMember{
		Name:   name,
		Status: member.Status + offsetStatus,
	}
	if p != nil {
		p.Total++
		current.Index = p.Total
	}
	if member.Spouse != nil {
		wn := []rune(member.Spouse.Person.Name)
		current.WifeNames = append(current.WifeNames, wn)
		for _, sp := range member.Spouses {
			wn := []rune(sp.Person.Name)
			current.WifeNames = append(current.WifeNames, wn)
		}
	}
	for _, child := range member.Children {
		c := cbp.MemberConv(child, offsetStatus, p)
		current.Children = append(current.Children, c)
	}
	return current
}

func (cbp *ClanBookPrinter) deepCal(current *PdfMember) {
	// 进度计算
	if cbp.progress != nil {
		if cbp.progress.closed {
			return
		}
		select {
		case cbp.progress.Message <- ProgressMessage{Step: 1, Message: "深度计算", Index: current.Index}:
		case <-cbp.progress.cancel:
			cbp.progress.close()
			return
		}
	}
	if cbp.lastStatus < current.Status {
		cbp.lastStatus = current.Status
	}
	current.MinSize.Width = float64(cbp.MinStyle.FontSize) + cbp.Padding.Left + cbp.Padding.Right
	current.MinSize.Height = float64(len(current.Name)*cbp.MinStyle.FontSize) + cbp.Padding.Top + cbp.Padding.Buttom
	current.NorSize.Width = float64(cbp.NorStyle.FontSize) + cbp.Padding.Left + cbp.Padding.Right
	current.NorSize.Height = float64(len(current.Name)*cbp.NorStyle.FontSize) + cbp.Padding.Top + cbp.Padding.Buttom
	current.MaxSize.Width = float64(cbp.MaxStyle.FontSize) + cbp.Padding.Left + cbp.Padding.Right
	current.MaxSize.Height = float64(len(current.Name)*cbp.MaxStyle.FontSize) + cbp.Padding.Top + cbp.Padding.Buttom
	whMin := 0.0
	whNor := 0.0
	whMax := 0.0
	for _, wn := range current.WifeNames {
		wnhMin := cbp.WifeNamesIndent + float64(len(wn)*cbp.MinStyle.FontSizeForWife)
		if whMin < wnhMin {
			whMin = wnhMin
		}
		wnhNor := cbp.WifeNamesIndent + float64(len(wn)*cbp.NorStyle.FontSizeForWife)
		if whNor < wnhNor {
			whNor = wnhNor
		}
		wnhMax := cbp.WifeNamesIndent + float64(len(wn)*cbp.MaxStyle.FontSizeForWife)
		if whMax < wnhMax {
			whMax = wnhMax
		}
	}
	if current.MinSize.Height < whMin {
		current.MinSize.Height = whMin
	}
	if current.NorSize.Height < whNor {
		current.NorSize.Height = whNor
	}
	if current.MaxSize.Height < whMax {
		current.MaxSize.Height = whMax
	}

	current.MinSize.Height += cbp.LineHeight
	current.NorSize.Height += cbp.LineHeight
	current.MaxSize.Height += cbp.LineHeight
	current.MinSize.Width += float64(len(current.WifeNames) * cbp.MinStyle.FontSizeForWife)
	current.NorSize.Width += float64(len(current.WifeNames) * cbp.NorStyle.FontSizeForWife)
	current.MaxSize.Width += float64(len(current.WifeNames) * cbp.MaxStyle.FontSizeForWife)

	for _, child := range current.Children {
		cbp.deepCal(child)
	}
}

func (cbp *ClanBookPrinter) newClanPage() *clanPage {

	cbp.currentPageIndex++
	cbp.pages[cbp.currentPageIndex] = &clanPage{
		index: cbp.currentPageIndex,
		hor: area{
			startX: cbp.PagePadding.Left,
			startY: cbp.PagePadding.Top,
			width:  cbp.pageLong - cbp.PagePadding.Left - cbp.PagePadding.Right,
			height: cbp.pageShort - cbp.PagePadding.Top - cbp.PagePadding.Buttom,
		},
		ver: area{
			startX: cbp.PagePadding.Left,
			startY: cbp.PagePadding.Top,
			width:  cbp.pageShort - cbp.PagePadding.Left - cbp.PagePadding.Right,
			height: cbp.pageLong - cbp.PagePadding.Top - cbp.PagePadding.Buttom,
		},
	}
	currentPage := cbp.pages[cbp.currentPageIndex]
	return currentPage
}

// calForRect 计算终止位置
func (cbp *ClanBookPrinter) calStatusHeightForRect(pm *PdfMember, rect *memberRect) {
	if rect == nil {
		panic("calcurate with nil rect")
	}

	// 取最高的作为当前世代高度
	if rect.statusHeight[pm.Status] < pm.NorSize.Height {
		rect.statusHeight[pm.Status] = pm.NorSize.Height
	}

	if pm.Children == nil || len(pm.Children) == 0 {
		return
	}

	outOfBox := false
	rectHeight := rect.calStatusHeight(pm.Status)
	for _, child := range pm.Children {
		ch := child.NorSize.Height
		if ch < rect.statusHeight[child.Status] {
			ch = rect.statusHeight[child.Status]
		}
		cw := rect.member.PrintWidth(child.Status, FNor)
		if rect.direction == horizonal {
			if rectHeight+ch+float64(cbp.FontSizeForNum)*2 > cbp.pageShort-cbp.PagePadding.Top-cbp.PagePadding.Buttom { // 当高度到达极限值时
				if rectHeight/rect.norWidth > 1.4 {
					rect.direction = vertical
					break
				} else {
					outOfBox = true
					break
				}
			} else if cw > cbp.pageLong-cbp.PagePadding.Left-cbp.PagePadding.Right { // 当宽度到达极限值时
				outOfBox = true
				break
			}
		} else {
			if (rectHeight+ch+float64(cbp.FontSizeForNum)*2 > cbp.pageLong-cbp.PagePadding.Top-cbp.PagePadding.Buttom) || cw > cbp.pageShort-cbp.PagePadding.Left-cbp.PagePadding.Right {
				outOfBox = true
				break
			}
		}
	}
	if !outOfBox {
		for _, child := range pm.Children {
			cbp.calStatusHeightForRect(child, rect)
		}
	}
}

// 计算世代文字长度
func (cbp *ClanBookPrinter) statusTextWidth(status int32) float64 {
	return float64(cbp.NorStyle.FontSizeForStatus * len([]rune(strconv.Itoa(int(status)))))
}

// designRect 设计族谱块 然后设计 页面
// twice 用于设计第二次
func (cbp *ClanBookPrinter) designRect(pm *PdfMember, rect *memberRect) {
	if cbp.progress != nil {
		if cbp.progress.closed {
			return
		}
		select {
		case cbp.progress.Message <- ProgressMessage{Step: 2, Message: "设计族谱信息块", Index: pm.Index}:
		case <-cbp.progress.cancel:
			cbp.progress.close()
			return
		}
	}
	if rect == nil {
		rect = &memberRect{
			member:       pm,
			statusHeight: make(map[int32]float64),
		}
		cbp.calStatusHeightForRect(pm, rect)
		found := false
		for _, q := range cbp.queue {
			if q.status == pm.Status {
				q.mrs = append(q.mrs, rect)
				found = true
				break
			}
		}
		if !found {
			cbp.queue = append(cbp.queue, &rectQueue{status: pm.Status, mrs: []*memberRect{rect}})
		}
	}

	// if rect.statusHeight[pm.Status] < pm.NorSize.Height {
	// 	rect.statusHeight[pm.Status] = pm.NorSize.Height
	// }

	if rect.toStatus < pm.Status {
		rect.toStatus = pm.Status
	}
	if pm.Children == nil || len(pm.Children) == 0 {
		return
	}

	outOfBox := false
	rectHeight := rect.calStatusHeight(pm.Status)
	for _, child := range pm.Children {
		ch := child.NorSize.Height
		if ch < rect.statusHeight[child.Status] {
			ch = rect.statusHeight[child.Status]
		}
		cw := rect.member.PrintWidth(child.Status, FNor)
		if rect.direction == horizonal {
			// FIXME: 当所有子树都遍历完了才知道高度
			if rectHeight+ch+float64(cbp.FontSizeForNum)*2 > cbp.pageShort-cbp.PagePadding.Top-cbp.PagePadding.Buttom { // 当高度到达极限值时
				if rectHeight/rect.norWidth > 1.4 {
					rect.direction = vertical
					break
				} else {
					outOfBox = true
					break
				}
			} else if cw > cbp.pageLong-cbp.PagePadding.Left-cbp.PagePadding.Right { // 当宽度到达极限值时
				outOfBox = true
				break
			}
		} else {
			if (rectHeight+ch+float64(cbp.FontSizeForNum)*2 > cbp.pageLong-cbp.PagePadding.Top-cbp.PagePadding.Buttom) || cw > cbp.pageShort-cbp.PagePadding.Left-cbp.PagePadding.Right {
				outOfBox = true
				break
			}
		}
	}
	if outOfBox {
		cbp.designRect(pm, nil)
	} else {
		for _, child := range pm.Children {
			cbp.designRect(child, rect)
		}
	}

}

func (cbp *ClanBookPrinter) dispatchPage() {
	sort.Sort(cbp.queue)
	currentPage := cbp.newClanPage()
	for _, q := range cbp.queue {
		for _, mr := range q.mrs {
			width := mr.member.PrintWidth(mr.toStatus, FNor) + cbp.statusTextWidth(mr.toStatus) + cbp.PagePadding.Left + cbp.Padding.Right
			height := mr.calStatusHeight(mr.toStatus) + cbp.PagePadding.Top + cbp.PagePadding.Buttom

		NEW:
			if mr.direction == vertical {
				if currentPage.pointX == 0 && currentPage.pointY == 0 {
					// 新的竖向页面
					currentPage.direction = vertical
					mr.startX = currentPage.ver.startX
					mr.startY = currentPage.ver.startY
					mr.member.Anchor = &Anchor{pageIndex: cbp.currentPageIndex}
					currentPage.mrs = append(currentPage.mrs, mr)
					currentPage.pointX = mr.startX + width
					currentPage.pointY = mr.startY + height

					currentPage.hor = area{
						startX: mr.startX,
						startY: mr.startY + height,
						width:  cbp.pageShort - cbp.PagePadding.Right - mr.startX,
						height: cbp.pageLong - cbp.Padding.Buttom - mr.startY - height,
					}
					currentPage.ver = area{
						startY: mr.startY,
						startX: mr.startX + width,
						width:  cbp.pageShort - cbp.Padding.Right - mr.startX - width,
						height: cbp.pageLong - cbp.Padding.Buttom - mr.startY - height,
					}

				} else {
					if currentPage.hor.width > width && currentPage.hor.height > height {
						// 竖 使用 下面剩余部分
						area := currentPage.hor
						mr.startX = area.startX
						mr.startY = area.startY
						mr.member.Anchor = &Anchor{pageIndex: cbp.currentPageIndex}
						currentPage.mrs = append(currentPage.mrs, mr)
						x := mr.startX + width
						if x > currentPage.pointX {
							currentPage.pointX = x
							currentPage.hor.startX = x
							currentPage.hor.width = cbp.pageShort - cbp.PagePadding.Right - x
							currentPage.ver.startX = x
							currentPage.ver.width = cbp.pageShort - cbp.PagePadding.Right - x
						} else {
							currentPage.hor.startX = x
							currentPage.hor.width = cbp.pageShort - cbp.Padding.Right - x
						}

					} else if currentPage.ver.width > width && currentPage.ver.height > height {
						// 竖 使用 右边剩余部分
						area := currentPage.ver
						mr.startX = area.startX
						mr.startY = area.startY
						mr.member.Anchor = &Anchor{pageIndex: cbp.currentPageIndex}
						currentPage.mrs = append(currentPage.mrs, mr)
						y := mr.startY + height
						if y > currentPage.pointY {
							currentPage.pointY = y
							currentPage.ver.startY = y
							currentPage.ver.height = cbp.pageLong - cbp.PagePadding.Buttom - y
							currentPage.hor.width = cbp.pageShort - cbp.PagePadding.Right - currentPage.pointX
						} else {
							currentPage.ver.startY = y
							currentPage.ver.height = cbp.pageLong - cbp.Padding.Buttom - y
						}
					} else {
						currentPage = cbp.newClanPage()
						goto NEW
					}
				}

			} else {
				if currentPage.pointX == 0 && currentPage.pointY == 0 {
					// 新的横向页面
					mr.startX = currentPage.hor.startX
					mr.startY = currentPage.hor.startY
					mr.member.Anchor = &Anchor{pageIndex: cbp.currentPageIndex}
					currentPage.mrs = append(currentPage.mrs, mr)
					currentPage.pointX = mr.startX + width
					currentPage.pointY = mr.startY + height
					currentPage.hor = area{
						startX: mr.startX,
						startY: mr.startY + height,
						width:  cbp.pageLong,
						height: cbp.pageShort - cbp.PagePadding.Buttom - height,
					}
					currentPage.ver = area{
						startY: mr.startY,
						startX: mr.startX + width,
						width:  cbp.pageLong - cbp.Padding.Right - mr.startX - width,
						height: cbp.pageShort - cbp.Padding.Buttom - mr.startY,
					}
				} else {
					if currentPage.hor.width > width && currentPage.hor.height > height {
						// 横 使用 下面剩余部分
						area := currentPage.hor
						mr.startX = area.startX
						mr.startY = area.startY
						mr.member.Anchor = &Anchor{pageIndex: cbp.currentPageIndex}
						currentPage.mrs = append(currentPage.mrs, mr)
						x := mr.startX + width + cbp.PagePadding.Left
						if x > currentPage.pointX {
							currentPage.pointX = x
							currentPage.hor.startX = x
							currentPage.hor.width = cbp.pageShort - cbp.PagePadding.Right - x
							currentPage.ver.startX = x
							currentPage.ver.width = cbp.pageShort - cbp.Padding.Right - x
						} else {
							currentPage.hor.startX = x
							currentPage.hor.width = cbp.pageShort - cbp.Padding.Right - x
						}
					} else if currentPage.ver.width > width && currentPage.ver.height > height {
						// 横 使用 右边剩余部分
						area := currentPage.ver
						mr.startX = area.startX
						mr.startY = area.startY
						mr.member.Anchor = &Anchor{pageIndex: cbp.currentPageIndex}
						currentPage.mrs = append(currentPage.mrs, mr)
						y := mr.startY + height + cbp.PagePadding.Top
						if y > currentPage.pointY {
							currentPage.pointY = y
							currentPage.ver.startY = y
							currentPage.ver.height = cbp.pageLong - cbp.Padding.Buttom - y
							currentPage.hor.width = cbp.pageShort - cbp.Padding.Right - currentPage.pointX
						} else {
							currentPage.ver.startY = y
							currentPage.ver.height = cbp.pageLong - cbp.Padding.Buttom - y
						}
					} else {
						currentPage = cbp.newClanPage()
						goto NEW
					}
				}
			}
		}
	}
}

func (cbp *ClanBookPrinter) printPages() {
	for i := cbp.pageStartNum; i <= cbp.currentPageIndex; i++ {
		page := cbp.pages[i]
		if page.direction == horizonal {
			cbp.pdf.AddPageWithOption(gopdf.PageOption{PageSize: &gopdf.Rect{H: 595, W: 842}})
		} else {
			cbp.pdf.AddPageWithOption(gopdf.PageOption{PageSize: &gopdf.Rect{H: 842, W: 595}})
		}

		for _, mr := range page.mrs {
			// 世代打印
			err := cbp.pdf.SetFont("阿里", "", cbp.NorStyle.FontSizeForStatus)
			if err != nil {
				panic(err)
			}
			lastY := mr.startY + cbp.Padding.Top
			for i := mr.member.Status; i <= mr.toStatus; i++ {
				lastY += mr.statusHeight[i-1]
				cbp.pdf.SetX(mr.startX + cbp.Padding.Left)
				cbp.pdf.SetY(lastY + cbp.LineHeight)
				cbp.pdf.Cell(nil, fmt.Sprintf("%d%s", i, cbp.statusName))
			}
			cbp.PrintMember(mr.member, mr.startX+cbp.PagePadding.Top+float64(cbp.NorStyle.FontSizeForStatus*2)+10, mr.startY, page.index, mr.statusHeight, mr.toStatus, FNor)
		}
	}
}

// PrintMember 打印设计好的族人信息
func (cbp *ClanBookPrinter) PrintMember(pm *PdfMember, currentX, currentY float64, pageIndex int, statusHeight map[int32]float64, stopStatus int32, typ StyleType) {
	if cbp.progress != nil {
		if cbp.progress.closed {
			return
		}
		select {
		case cbp.progress.Message <- ProgressMessage{Step: 3, Message: "写入文件", Index: pm.Index}:
		case <-cbp.progress.cancel:
			cbp.progress.close()
			return
		}
	}
	wwc := pm.PrintWidth(stopStatus, typ)
	style := cbp.NorStyle
	size := pm.NorSize
	switch typ {
	case FMin:
		style = cbp.MinStyle
		size = pm.MinSize
	case FMax:
		style = cbp.MaxStyle
		size = pm.MaxSize
	}

	x := currentX + (wwc / 2) - (size.Width / 2)

	if pm.Status != stopStatus && pm.Anchor != nil && pm.Anchor.ahchorIndex != 0 {
		// if pm.Anchor.ahchorIndex == 0 {
		// 	panic("anchor's index is not point")
		// }
		cbp.useFontForPageNum()
		cbp.pdf.SetGrayFill(0.3)
		nl := len([]rune(strconv.Itoa(pm.Anchor.ahchorIndex))) + 3
		cbp.pdf.SetX(x + size.Width/2 - float64(nl*cbp.FontSizeForNum)/2)
		cbp.pdf.SetY(currentY)
		cbp.pdf.Cell(nil, fmt.Sprintf("上接%d页", pm.Anchor.ahchorIndex))
		cbp.pdf.SetGrayFill(0)
	}

	cbp.useFont(style)
	for i, n := range pm.Name {
		y := currentY + float64(i*style.FontSize)
		cbp.pdf.SetX(x + cbp.Padding.Left)
		cbp.pdf.SetY(y + cbp.Padding.Top + cbp.Padding.Buttom)
		cbp.pdf.Cell(nil, string(n))
	}

	cbp.useFontForWife(style)
	for wi, ns := range pm.WifeNames {
		wx := x + cbp.Padding.Right + float64(style.FontSize) + float64(wi*style.FontSizeForWife)
		for i, n := range ns {
			cbp.pdf.SetX(wx)
			cbp.pdf.SetY(currentY + cbp.Padding.Top + cbp.WifeNamesIndent + float64(i*style.FontSizeForWife))
			cbp.pdf.Cell(nil, string(n))
		}
	}

	if pm.Status == stopStatus {
		if pm.Anchor != nil {
			// 并设置锚点所在页码
			pm.Anchor.ahchorIndex = pageIndex
			cbp.useFontForPageNum()
			cbp.pdf.SetGrayFill(0.3)
			nl := len([]rune(strconv.Itoa(pm.Anchor.pageIndex))) + 2
			cbp.pdf.SetX(currentX + (size.Width / 2) - float64(nl*cbp.FontSizeForNum)/2)
			cbp.pdf.SetY(currentY + size.Height)
			cbp.pdf.Cell(nil, fmt.Sprintf("第%d页", pm.Anchor.pageIndex))
			cbp.pdf.SetGrayFill(0)
		}
		return
	}

	if len(pm.Children) > 0 {
		lineY := currentY + size.Height + cbp.Padding.Buttom
		cbp.DrawLongitudeLine(currentX+(wwc/2), lineY)
		// 横线值捕获
		hlX := currentX
		toX := currentX
		// 打印子成员
		cx := currentX
		for i, child := range pm.Children {
			wwc := child.PrintWidth(stopStatus, typ)
			if i == 0 {
				hlX += wwc / 2
			}
			if i == len(pm.Children)-1 {
				toX += wwc / 2
			} else {
				toX += wwc
			}
			cbp.PrintMember(child, cx, currentY+statusHeight[pm.Status], pageIndex, statusHeight, stopStatus, typ)
			cx += wwc
		}
		if len(pm.Children) > 1 {
			cbp.DrawHorizontalLine(hlX, lineY, toX)
			cbp.DrawLongitudeLine(hlX, lineY+5)
			cbp.DrawLongitudeLine(toX, lineY+5)
		}

	}

}

// DrawLongitudeLine 往回画竖线
func (cbp *ClanBookPrinter) DrawLongitudeLine(currentX, currentY float64) {
	cbp.pdf.SetStrokeColor(255, 0, 51)
	cbp.pdf.Line(currentX, currentY, currentX, currentY-cbp.LineHeight)
}

// DrawHorizontalLine 画横线
func (cbp *ClanBookPrinter) DrawHorizontalLine(currentX, currentY, toX float64) {
	cbp.pdf.SetStrokeColor(255, 0, 51)
	cbp.pdf.Line(currentX, currentY, toX, currentY)
}
