package models

// PrintProgress 打印进度
type PrintProgress struct {
	Total   int
	closed  bool
	cancel  chan struct{}
	done    chan struct{}
	Message chan ProgressMessage
}

// CreatePrintProgress for clan book making
func CreatePrintProgress() *PrintProgress {
	return &PrintProgress{
		cancel:  make(chan struct{}),
		done:    make(chan struct{}),
		Message: make(chan ProgressMessage),
	}
}

func (pp *PrintProgress) close() {
	pp.closed = true
	close(pp.Message)
	close(pp.done)
}

// Cancel making clan book
func (pp *PrintProgress) Cancel() {
	if !pp.closed {
		close(pp.cancel)
	}
}

func (pp *PrintProgress) Done() <-chan struct{} {
	d := pp.done
	return d
}

type ProgressMessage struct {
	Step    uint //步骤
	Message string
	Index   int
}
