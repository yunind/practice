package models

import "database/sql"

import "gitee.com/yunind/practice/robotgo/pos/servers/product/models/util"

import "log"

// Product 商品
type Product struct {
	ItemNo    string
	ItemSubno string
	SalePrice float64
}

func GetTestProducts() []*Product {
	return []*Product{
		&Product{
			ItemNo:    "020838",
			ItemSubno: "6901028001618",
			SalePrice: 17,
		},
		&Product{
			ItemSubno: "6953125352124",
		},
	}
}

func GetProductsFromSQL(path string) []*Product {
	c, err := util.ReadYAML(path)
	if err != nil {
		panic(err)
	}
	db, err := sql.Open("odbc", c.ODBCFormat())
	if err != nil {
		panic(err)
	}
	defer db.Close()

	rows, err := db.Query(`SELECT TOP 25
	[item_no]
	,[item_subno]
	,[sale_price] FROM [kmjxc_pro].[dbo].[pos_t_saleflow] GROUP BY [item_no],[item_subno],[sale_price],[trade_date] HAVING [trade_date] >= ?`, "20200122")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var ps []*Product
	for rows.Next() {
		p := Product{}
		err := rows.Scan(&p.ItemNo, &p.ItemSubno, &p.SalePrice)
		if err != nil {
			panic(err)
		}
		ps = append(ps, &p)
	}
	log.Println("get data from sql")
	return ps
}
