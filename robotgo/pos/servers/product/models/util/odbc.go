package util

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config the configuration for mssql server
type Config struct {
	AppName  string `yaml:"AppName"`
	Server   string `yaml:"Server"`
	User     string `yaml:"User"`
	Password string `yaml:"Password"`
	Database string `yaml:"Database"`
	Typ      string `yaml:"Type"`
}

func ReadYAML(path string) (*Config, error) {
	fb, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	conf := Config{}
	err = yaml.Unmarshal(fb, &conf)
	if err != nil {
		return nil, err
	}

	return &conf, nil
}

// ODBCFormat the configuration with old odbc
// require import _ "github.com/alexbrainman/odbc"
func (conf Config) ODBCFormat() string {
	return fmt.Sprintf(
		"DSN=%s;Database=%s;UID=%s;PWD=%s",
		conf.Server,
		conf.Database,
		conf.User,
		conf.Password,
	)
}
