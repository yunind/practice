package main

import (
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	"gitee.com/yunind/practice/robotgo/pos/servers/product/models"
	_ "github.com/alexbrainman/odbc"
	"github.com/go-vgo/robotgo"
)

func main() {
	rootPath, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	robotgo.Sleep(5)
	ps := models.GetProductsFromSQL(filepath.Join(rootPath, "config.yaml"))
	for {
		robotgo.Sleep(10)
		rand.Seed(time.Now().UnixNano())
		// 随机商品数
		pn := rand.Intn(10)
		for i := 0; i < pn; i++ {
			// 从商品列表中随机选择商品
			rn := rand.Intn(len(ps))
			p := ps[rn]
			robotgo.TypeStr(p.ItemSubno)
			robotgo.Sleep(1)
			// 录入条码
			log.Println("录入条码", p.ItemSubno)
			robotgo.KeyTap("enter")
		}
		// 确认订单
		log.Println("确认订单")
		robotgo.KeyTap("enter")
		robotgo.Sleep(2)
		// 直接付款
		log.Println("直接付款")
		robotgo.KeyTap("enter")
	}
}
